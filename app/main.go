package main

import (
	"fmt"
	"log"
	"os"

	"database/sql"

	_ "github.com/go-sql-driver/mysql"
)

type db_info struct {
	Host     string
	User     string
	Password string
	Port     string
}

func main() {
	gamedata := db_info{
		Host:     os.Getenv("GAMEDATA_HOST"),
		User:     os.Getenv("GAMEDATA_USER"),
		Password: os.Getenv("GAMEDATA_PASSWORD"),
		Port:     os.Getenv("GAMEDATA_PORT"),
	}

	connStr := fmt.Sprintf("%s:%s@tcp(%s:%s)/gamedata", gamedata.User, gamedata.Password, gamedata.Host, gamedata.Port)

	db, err := sql.Open("mysql", connStr)

	if err != nil {
		log.Panic(err)
	}

	err = db.Ping()
	if err == nil {
		log.Println("Database connected successfully.")
	}

	defer db.Close()
}
